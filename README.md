# kuangjia_demo

#### 介绍
selenium业务逻辑层+pytest执行+allure报告的UI自动化框架

1、本框架采用业务逻辑和业务逻辑区分存放的思想，便于ui脚本的维护
2、每个文件中有较为详细的注释，便于理解


注意：需要自行下载allure插件   
allure==2.28.0    (url:https://github.com/allure-framework/allure2/releases/tag/2.28.0)

#### 软件架构
- CaseElements ---用于存放case元素位置（位置存放采用By格式）
- Config ---一些基本公共方法
    - Pages
        - Edge ---edge浏览器驱动存放位置
        - Chromedriver ---谷歌浏览器驱动存放
        - BasePages.py ---封装selenium基础操作的包
    - Business.py ---用于存放公共业务模块（重复的公共业务 如：UI自动化中的登录操作）
    - BusinessElements.py ---用于存放业务公共逻辑模块的元素位置（位置存放采用By格式）
    - globalVars.py ---框架需要用到的一些位置（内含大量无用代码）
    - Yamlread.py ---yaml文件读取功能
- test_case ---case存放位置
- Taskkill.py ---手动结束进程文件（场景执行失败后，后台进程不会自行结束，所以无聊的时候可以结束进程释放电脑运行）
- conftest.py ---框架的一些公共方法 比如浏览器驱动初始化
- pytest.ini
- pytest.pdf ---pytset官方文档
- pytest中文文档.pdf ---pytset官方文档
- run.py ---执行文件（编写了多个执行文件，看个人需求进行使用）
- run_dubug.py ---调试执行文件
- run_one.py ---执行文件
- run_two.py ---执行文件
- run_three.py ---执行文件


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)

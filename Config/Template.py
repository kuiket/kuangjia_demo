# -*- coding: utf-8 -*-
import pandas as pd
import os

# def test_create_template(GetFilePath):

current_dir = os.path.dirname(os.path.abspath(__file__))  # 获取当前文件所在目录
parent_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))  # 根目录

template_path = f'{current_dir}/template'

os.makedirs(template_path)

ui_template = pd.DataFrame(
    {
        'function': ['click', 'send_keys', 'screenshot', 'quit'],
        'loc': ['//@id="xxx"', '//@id="xxx"', '', ''],
        'keyword': ['', 'aaaaaa', '', ''],
        '需要啥字段需要自己加': ['content', 'function 为必填', '内容', '内容'],

    }
)

api_template = pd.DataFrame(
    {
        'method': ['post', 'get', 'post', 'get'],
        'url': ['https://apis.eolink.com/api/common/User/getUserInfo',
                'https://service.apispace.com/apibee/recommendwordconfig/list',
                'https://service.apispace.com/userCenter/common/third-party/qr-check', 'www.baidu.com'],
        'params': ['', '', '', ''],
        'data': ['', '', '', '内容'],
        'json': ["""{
  "type": "customizeCodeException",
  "statusCode": "200001",
  "errorMsg": "Header Missing Authorization"
}""", '', """{"code":"9419d43c04174e27a58738a99b85b969"}""", '{"aa":"bb"}'],
        'headers': ['', '', '', 'token'],
        'cookies': ['', '', '', ''],
        'auth': ['', '', '', ''],
        'timeout': ['', '', '', ''],
        'allow_redirects': ['', '', '', ''],
        'proxies': ['', '', '', ''],
        'verify': ['', '', '', ''],
        'stream': ['', '', '', ''],
        'cert': ['', '', '', ''],

    }
)


def create_template():
    os.makedirs(template_path)
    # 使用ExcelWriter生成Excel文件
    with pd.ExcelWriter(f'{template_path}/UI_case.xlsx', engine='openpyxl') as writer:
        ui_template.to_excel(writer, sheet_name='UIcase模板', index=False)
    with pd.ExcelWriter(f'{template_path}/API_case.xlsx', engine='openpyxl') as writer:
        api_template.to_excel(writer, sheet_name='APIcase模板', index=False)

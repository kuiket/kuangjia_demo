import time, logging, logging.handlers, datetime
import os


class Log:
    Time = time.strftime("%m_%d_%H_%M_%S_")
    current_dir = os.path.dirname(os.path.abspath(__file__))  # 获取当前的路径

    # def log(self):
    #     """
    #
    #     :return:
    #     """
    #     Stream = open(f"./log/{self.time}my.log", encoding="utf-8", mode="a")   #  日志名称+路径
    #     # Filename = f'./Outputs/log/{self.time}my.txt'       #  日志名称+路径
    #     LOG_FORMAT = "%(asctime)s - %(levelname)s - %(message)s"    # 日志输出的格式
    #     DATE_FORMAT = "%m/%d/%Y %H:%M:%S %p"    # 时间格式
    #     logging.basicConfig(stream=Stream, level=logging.INFO, format=LOG_FORMAT, datefmt=DATE_FORMAT)
    #     return logging

    def log1(self):
        Filename = f'{self.current_dir}\log\{self.Time}详细日志.log'
        LOG_FORMAT = "%(asctime)s - %(levelname)s - %(message)s"
        DATE_FORMAT = "%m/%d/%Y %H:%M:%S %p"
        logging.basicConfig(filename=Filename, level=logging.DEBUG, format=LOG_FORMAT, datefmt=DATE_FORMAT)
        return logging

    def log2(self):
        logger = logging.getLogger('mylogger')
        logger.setLevel(logging.INFO)
        LogName = f'{self.current_dir}\log\{self.Time}log.log'
        rf_handler = logging.handlers.TimedRotatingFileHandler(LogName,
                                                               interval=1,
                                                               backupCount=7, encoding='utf-8',
                                                               atTime=datetime.time(0, 0, 0, 0))
        rf_handler.setFormatter(logging.Formatter("%(asctime)s - %(levelname)s - %(message)s"))
        logger.addHandler(rf_handler)

        return logging

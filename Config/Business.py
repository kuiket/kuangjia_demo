# # -*- coding: utf-8 -*-

from Config.Pages.BasePages import BasePage
from Config.BusinessElements import Elements as ele
"""
用于存放一些业务逻辑的公共模板
例如：登录模块

初衷：为了减少代码量，减少重复流程的代码编写
"""


# 业务公共逻辑模块
class business(BasePage):

    def login(self):
        # 假装登录流程
        self.send_keys(ele.shurukuang1, "测试")  # 对输入框输入内容
        self.click(ele.baiduyixia)  # 对搜索按钮点击
        pass

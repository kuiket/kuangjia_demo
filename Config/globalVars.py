# coding: utf-8
import os
import datetime
import sys


class GlobalVars(object):
    # ---------------- 项目根目录 --------------------
    BASE_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    # ---------------- 隐式等待时间 --------------------
    IMPLICITLY_WAIT_TIME = 5

    # ---------------- 测试报告 --------------------
    REPORT_PATH = os.path.join(BASE_PATH, "report")
    REPORT_RESULT_PATH = os.path.join(REPORT_PATH, "allure_result")
    REPORT_END_PATH = os.path.join(REPORT_PATH, "allure_report")
    REPORT_HISTORY_PATH = os.path.join(REPORT_PATH, "allure_report", "history")

    # ---------------- 日志相关 --------------------
    # 日志级别
    LOG_LEVEL = 'debug'
    LOG_STREAM_LEVEL = 'debug'  # 屏幕输出流
    LOG_FILE_LEVEL = 'info'  # 文件输出流
    # 日志命名
    LOG_FOLDER = os.path.join(BASE_PATH, 'logs')
    LOG_FILE_NAME = os.path.join(LOG_FOLDER, datetime.datetime.now().strftime('%Y-%m-%d') + '.log')

    # ---------------- 邮件相关 --------------------
    # 邮件文件列表
    FILE_LIST = [
        os.path.join(BASE_PATH, "report", "zip", "report.zip")
    ]

    # ---------------- 压缩文件相关 --------------------
    # 要压缩文件夹的根路径
    REPORT_DIR = os.path.join(BASE_PATH, "report", "allure_report")

    # ---------------- bat文件相关 --------------------
    BAT_FILE = os.path.join(BASE_PATH, "bat", "generate_report.bat")
    RUN_SERVER_FILE = os.path.join(BASE_PATH, "bat", "run_allure_server.bat")
    ADD_REPORT_TO_GIT_FILE = os.path.join(BASE_PATH, "bat", "add_report_to_git.bat")

    # ---------------- Email相关 --------------------
    EMAIL_FROMADDR = 'xxxxxxxxxx@xx.com'  # 发件人邮箱
    EMAIL_PASSWORD = 'xxxxxxxxxx'  # 发件人授权码
    EMAIL_TOADDR = [  # 收件人地址列表
        'xxxxxxxxxx@qq.com'
    ]

    # ---------------- 截图相关 --------------------
    SCREENSHOT_DIR = os.path.join(BASE_PATH, "screenshot")

    # ---------------- MYSQL相关 -------------------
    DB_CONFIG = {
        "master": {
            "host": "10.88.0.13",
            "port": 33066,
            "user": "root",
            "password": "test123456",
            "db": "hlhz_oa_new",
            "charset": "utf8mb4"
        }
    }

    # --------------- 驱动相关 ----------------------
    # selenium驱动存放地址
    resource_path = os.path.join(BASE_PATH, "resource")
    web_driver_path = os.path.join(resource_path, "webdriver")
    ELEMENT_PATH = os.path.join(os.path.dirname(web_driver_path), "PageElement")
    # 默认为CHROME， IE后续可能会适配，但是IE适配难度较高，暂时不考虑
    browser = "CHROME"
    # CHROME版本，自动采集
    browser_version = None
    # selenium 驱动包TaoBao 镜像站
    ie_driver_url = "https://npm.taobao.org/mirrors/selenium/"
    driver_url = "https://npm.taobao.org/mirrors/chromedriver/"
    # 当前电脑平台，默认为mac，非mac设备运行时会自动改为windows或者linux(linux不适配，需考虑排除无界面设备）
    platform = "mac"  # 默认为mac
    kernel = sys.platform
    if "darwin" in kernel:
        # MAC os
        chrome_app = r"/Applications/Google\ Chrome.app/Contents/MacOS/"  # mac os chrome安装地址
    elif "win" in kernel:
        platform = "windows"
        # Win
        chrome_reg = r"SOFTWARE\Google\Chrome\BLBeacon"  # win chrome注册表地址
        instant_client = os.path.join(resource_path, "instant_client")
    else:
        platform = "linux"
        browser = "firefox"
    # 根据后续自动安装驱动返回
    DRIVER_PATH = None


G = GlobalVars()

if __name__ == '__main__':
    # print(G.SCREENSHOT_DIR)
    # print(G.kernel)
    # print(G.instant_client)
    # print(G.DRIVER_PATH)
    print(G.REPORT_RESULT_PATH)
    print(G.REPORT_END_PATH)
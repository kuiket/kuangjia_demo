from multiprocessing import Pool
import concurrent.futures
import pytest, time, os, shutil, subprocess

"""
多线程执行case  （未验证）
"""


# 命令模块
def run_parallel(theme):
    # pytest.main([f"--cmdopt={theme}", f"./test_case/{theme}", "--alluredir", "./allure/allure-results"])
    pytest.main([f"./test_case/{theme}", "--alluredir", "./allure/allure-results"])


# 用于执行指定的case
# themes = [['test_BaiDu.py', 'test_one.py'], ['test_BaiDu.py', 'test_one.py']]
themes = [['test_BaiDu.py', 'test_one.py'], ['test_BaiDu.py', 'test_one.py']]


def run_tests_with_multiprocessing(num_processes):
    """

    :param num_processes:  线程数
    :return:
    """
    with concurrent.futures.ProcessPoolExecutor(max_workers=num_processes) as executor:
        for theme in themes:
            executor.submit(subprocess.run, run_parallel(theme))

    # 整合到一份报告
    times = time.strftime("%m_%d_%H_%M_%S")
    path = f'./allure/{times}'
    os.system(f'allure generate ./allure/allure-results -o {path} --clean')


if __name__ == '__main__':
    import os

    """
    case失败之后或者是调试时，进程不会结束。所以新执行测试之前需要先结束进程
    """
    os.system('taskkill /im msedgedriver.exe /F')  # 执行case之前结束 edge驱动进程
    os.system('taskkill /im chromedriver.exe /F')  # 执行case之前结束 Chrome驱动进程
    run_tests_with_multiprocessing()

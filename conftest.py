# # -*- coding: utf-8 -*-
import os
import pytest
import allure
from Config.LOG import Log
from selenium import webdriver
import random

Log = Log()
log = Log.log2()


# pytest配置对象
# 注意此对象只能在顶层目录的conftest.py文件中完成
# action：store 存储参数
# default 参数默认值
# help 参数帮助信息，此处为无
def pytest_addoption(parser):
    parser.addoption("--cmdopt", action="store",
                     default="chrome",
                     help="将自定义命令行参数 '--cmdopt' 添加到 pytest 配置中")


# 从配置对象获取 cmdopt 的值
# 然后任何 fixture 或测试用例都可以调用 cmdopt 来获得设备信息
@pytest.fixture
def cmdopt(pytestconfig):
    return pytestconfig.getoption("--cmdopt")


@pytest.fixture
def gaga():
    return "oooooooooooooooo"


def get_file_path(fileName):
    """
    获取 文件的路径
    :param fileName: 文件名称（带后缀）
    :return: file_path 文件路径
    """
    # 获取当前文件所在目录
    current_dir = os.path.dirname(os.path.abspath(__file__))
    # 获取指定目录下所有文件的路径
    for root, dirs, files in os.walk(current_dir):
        for file in files:
            if file == fileName:
                # 拼接文件路径
                file_path = os.path.join(root, file)
                # 获取文件的绝对路径
                absolute_path = os.path.abspath(file_path)
                # 打印文件路径
                file_path = absolute_path.replace('\\', '\\\\')
                return file_path


@pytest.fixture(scope='function', autouse=False, name='Firefox')
def browser_Firefox():
    """
    选择Firefox火狐浏览器执行自动化
    :return:
    """

    # 识别驱动执行
    # firefoxPtah = get_file_path('geckodriver.exe')
    # driver = webdriver.Firefox(executable_path=firefoxPtah)

    # 无需驱动执行
    from webdriver_manager.firefox import GeckoDriverManager  # firefox
    from selenium.webdriver.firefox.service import Service as FirefoxService
    driver = webdriver.Firefox(service=FirefoxService(GeckoDriverManager().install()))

    driver.maximize_window()  # 浏览器最大化
    allure.step(f"打开{driver}浏览器")
    log.info(f"打开{driver}浏览器")
    yield driver  # 把driver带出 def函数  并退出def函数


@pytest.fixture(scope='function', autouse=False, name='Ie')
def browser_Ie():
    """
    选择ie浏览器执行自动化
    :return:
    """

    # 识别驱动执行
    # iePath = get_file_path("IEDriverServer.exe")  # ie的驱动还没下过  忘记名字了
    # driver = webdriver.Ie(executable_path=iePath.replace('\\', '\\\\'))

    # 无需驱动执行
    from webdriver_manager.microsoft import IEDriverManager  # ie
    from selenium.webdriver.ie.service import Service as IEService
    driver = webdriver.Ie(service=IEService(IEDriverManager().install()))

    driver.maximize_window()  # 浏览器最大化
    allure.step(f"打开{driver}浏览器")
    log.info(f"打开{driver}浏览器")
    yield driver  # 把driver带出 def函数  并退出def函数


@pytest.fixture(scope='function', autouse=False, name='Edge')
def browser_Edge():
    """
    选择Edge浏览器执行自动化
    :return:
    """

    # 识别驱动执行
    # edgePath = get_file_path("msedgedriver.exe")
    # driver = webdriver.Edge(executable_path=edgePath.replace('\\', '\\\\'))

    # 无需驱动执行
    from webdriver_manager.microsoft import EdgeChromiumDriverManager  # edge
    from selenium.webdriver.edge.service import Service as EdgeService
    driver = webdriver.Edge(service=EdgeService(EdgeChromiumDriverManager().install()))

    driver.maximize_window()  # 浏览器最大化
    allure.step(f"打开{driver}浏览器")
    log.info(f"打开{driver}浏览器")
    yield driver  # 把driver带出 def函数  并退出def函数


@pytest.fixture(scope='function', autouse=False, name='Chrome')
def browser_Chrome():
    """
    选择Chrome谷歌浏览器执行自动化
    :return:
    """

    # 识别驱动执行
    # chromePath = get_file_path("chromedriver.exe")
    # driver = webdriver.Chrome(executable_path=chromePath.replace('\\', '\\\\'))

    # 无需驱动执行
    from webdriver_manager.chrome import ChromeDriverManager  # Chrome
    from selenium.webdriver.chrome.service import Service as ChromeService
    driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()))

    driver.maximize_window()  # 浏览器最大化
    allure.step(f"打开{driver}浏览器")
    log.info(f"打开{driver}浏览器")
    yield driver  # 把driver带出 def函数  并退出def函数






if __name__ == '__main__':
    print("**********************************")
    # test()
    print("**********************************")

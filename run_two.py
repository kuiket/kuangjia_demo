import pytest,time
import os


"""
执行所有的case,test_case路径下以 test_ 开头的py文件
"""
current_dir = os.path.dirname(os.path.abspath(__file__))  # 获取当前项目的路径


# 命令汇总
def run_parallel():
    """
    被执行的命令
    :return:
    """
    # pytest.main(['-vs','-n','2','--reruns=2','./test_case/test_BaiDu.py'])
    pytest.main(['-vs', '-n', '4', '--reruns=2'])
    times = time.strftime("%m_%d_%H_%M_%S")
    path = f'{current_dir}/allure/{times}'  # 报告路径+名称
    os.system(f'allure generate {current_dir}/allure/allure-results -o {path} --clean')  # 统合当前线程报告命令


# 执行命令封装模块
def pytest_start():
    """
    挑选部分用例执行，只能固定执行 *.py 文件下所有case
    """
    run_parallel()


if __name__ == '__main__':
    pytest_start()
    # run_parallel()

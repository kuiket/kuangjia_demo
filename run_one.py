# -*- coding: utf-8 -*-
from multiprocessing import Pool
import pytest, time
import os

"""
执行指定文件的case    将需要执行的case文件写入 themes 数组中
"""
current_dir = os.path.dirname(os.path.abspath(__file__))  # 获取当前项目的路径

# 命令模块
def run_parallel(theme):
    # pytest.main([f"--cmdopt={theme}", f"./test_case/{theme}", "--alluredir", "./allure/allure-results"])
    pytest.main([f"./test_case/{theme}", "--alluredir", f"{current_dir}/allure/allure-results"])


# 用于执行指定的case
# themes = [['test_BaiDu.py', 'test_one.py'], ['test_BaiDu.py', 'test_one.py']]
themes = [['test_BaiDu.py', 'test_one.py'], ['test_BaiDu.py', 'test_one.py']]


def pytest_start():
    for theme in themes:
        with Pool(len(theme)) as pool:
            pool.map(run_parallel, theme)
            pool.close()
            pool.join()

    # 整合到一份报告
    times = time.strftime("%m_%d_%H_%M_%S")
    path = f'{current_dir}/allure/{times}'
    os.system(f'allure generate {current_dir}/allure/allure-results -o {path} --clean')


if __name__ == '__main__':
    import os

    """
    case失败之后或者是调试时，进程不会结束。所以新执行测试之前需要先结束进程
    """
    os.system('taskkill /im msedgedriver.exe /F')  # 执行case之前结束 edge驱动进程
    os.system('taskkill /im chromedriver.exe /F')  # 执行case之前结束 Chrome驱动进程
    pytest_start()

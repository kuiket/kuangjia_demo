# -*- coding: utf-8 -*-
import time
from CaseElements.BaiDuElements import BAIDU as ele
from Config.Business import business
from Config.Pages.BasePages import BasePage
from Config.LOG import Log
import pytest, allure


@allure.feature("baidu_class")  # 归为大类
class TestBaiDu(BasePage):
    LOG = Log()
    log = LOG.log2()
    Business = business()  # 公共模块

    url = "https://www.baidu.com/"

    def test_fangfa1(self, Chrome):
        allure.dynamic.severity(allure.severity_level.BLOCKER)
        allure.dynamic.title("方法1")  # case报告名称
        allure.dynamic.story("Case")  # 小类
        allure.dynamic.description("打开浏览器-输入测试-搜索-关闭浏览器")  # 描述
        try:
            self.driver = Chrome  # 必须要写
            self.get_url(self.url)  # 打开浏览器
            self.send_keys(ele.shurukuang1, "测试")  # 对输入框输入内容
            self.click(ele.baiduyixia)  # 对搜索按钮点击
            self.Business.login()  # 调用公共业务模块
            print("=========")

            self.screenshot("截图")
        except Exception as e:
            self.log.error(e)
        finally:
            self.quit()

    def test_fangfa2(self, Chrome):
        allure.dynamic.severity(allure.severity_level.BLOCKER)
        allure.dynamic.title("方法2")  # 报告名称
        allure.dynamic.story("Case1")  # 小类
        allure.dynamic.description("打开浏览器-输入测试-搜索-关闭浏览器")  # 描述
        try:
            self.driver = Chrome
            self.get_url("https://www.baidu.com")
            self.send_keys(ele.shurukuang1, "测试")
            self.send_keys(ele.shurukuang1, "12124124")
            self.send_keys(ele.shurukuang1, "22222222222222222")

        except Exception as e:
            self.log.error(e)
        finally:
            pass
            self.screenshot("截图")
            time.sleep(5)
            self.quit()


if __name__ == '__main__':
    pytest.main(['-vs'])

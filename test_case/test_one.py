# -*- coding: utf-8 -*-
import time
from CaseElements.BaiDuElements import BAIDU as ele
from Config.Pages.BasePages import BasePage
from Config.LOG import Log
import pytest, allure


@allure.feature("one_class")  # 归为大类
class TestBaiDu(BasePage):
    LOG = Log()
    log = LOG.log2()

    # 选择浏览器

    url = "https://www.baidu.com/"

    def test_fangfa1(self, Edge):
        allure.dynamic.severity(allure.severity_level.BLOCKER)
        allure.dynamic.title("方法1")  # 报告名称
        allure.dynamic.story("Case")  # 小类
        allure.dynamic.description("打开浏览器-输入测试-搜索-关闭浏览器")  # 描述

        self.driver = Edge
        self.get_url(self.url)  # 打开浏览器
        self.send_keys(ele.shurukuang1, "测试")  # 对输入框输入内容
        self.partial_screenshot(ele.shurukuang1, "指定截图")
        self.click(ele.baiduyixia)  # 对搜索按钮点击
        self.quit()  # 退出浏览器

    # @pytest.mark.usefixtures('Edge')
    def test_fangfa2(self, Edge):
        allure.dynamic.severity(allure.severity_level.BLOCKER)
        allure.dynamic.title("方法1")  # 报告名称
        allure.dynamic.story("Case")  # 小类
        allure.dynamic.description("打开浏览器-输入测试-搜索-关闭浏览器")  # 描述

        self.driver = Edge
        self.get_url("https://www.baidu.com")
        self.send_keys(ele.shurukuang1, "测试")
        self.send_keys(ele.shurukuang1, "12124124")
        self.send_keys(ele.shurukuang1, "111111111111111111111")
        time.sleep(5)
        self.quit()


if __name__ == '__main__':
    pytest.main(['-vs'])

# -*- coding: utf-8 -*-


import os

"""
case失败之后或者是调试时，进程不会结束。所以新执行测试之前需要先结束进程

"""
os.system('taskkill /im msedgedriver.exe /F')  # 执行case之前结束 edge驱动进程
os.system('taskkill /im msedgewebview2.exe /F')  # 执行case之前结束 edge驱动进程
os.system('taskkill /im iexplore.exe /F')  # 执行case之前结束 ie驱动进程
os.system('taskkill /im chromedriver.exe /F')  # 执行case之前结束 Chrome驱动进程

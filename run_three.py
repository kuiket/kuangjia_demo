import pytest, os, time

current_dir = os.path.dirname(os.path.abspath(__file__))  # 获取当前项目的路径

if __name__ == '__main__':
    '''
       -vs : 打印用例执行的详细信息
       -n ： 多进程运行（'-n', '2'）
       --reruns=nun : 用力失败重跑，nun:重跑次数
       ./test_case/test_BaiDu.py ： 需要执行的用例文件
       --alluredir ：生成测试报告所需要的数据，参数后面加上存放路径
       generate : 生成allure测试报告，用法：allure generate 测试数据路径 -o 报告的路径
       -o ：将测试数据输出到报告
       --clean ： 清除上次运行生成的测试报告
       :return:
       '''
    times = time.strftime("%m_%d_%H_%M_%S")
    path = f'{current_dir}/allure/{times}'
    pytest.main(['-vs', '-n', '2', '--reruns=2', '--alluredir', f'{current_dir}/allure/allure-results'])
    os.system(f'allure generate {current_dir}/allure/allure-results -o {path} --clean')
